#use wml::debian::translation-check translation="4c0996e191dd68b6dfbadfee3c048714d4cfa961" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen, qui
pourraient avoir pour conséquence un déni de service, une élévation de
privilèges du client à ceux de l'hôte ou des fuites d'informations.</p>

<p>En complément, cette mise à jour fournit des atténuations pour les
attaques spéculatives par canal auxiliaire <q>TSX Asynchronous Abort</q>.
Pour davantage d'informations, veuillez consulter
<a href="https://xenbits.xen.org/xsa/advisory-305.html">\
https://xenbits.xen.org/xsa/advisory-305.html</a>.</p>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 4.8.5.final+shim4.10.4-1+deb9u12. Notez que cela sera la
dernière mise à jour de sécurité pour Xen dans la distribution oldstable ;
la prise en charge amont pour la branche 4.8.x s'est achevée à la fin du
mois de décembre 2019. Si vous dépendez du suivi de sécurité pour votre
installation de Xen, une mise à jour vers la distribution stable (Buster)
est recommandée.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.11.3+24-g14b62ab3e5-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xen, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4602.data"
# $Id: $
