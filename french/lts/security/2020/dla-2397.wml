#use wml::debian::translation-check translation="e026257f08e526d1c102bd7a480750b606d6b2e2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans PHP, un langage de script embarqué
dans du HTML et côté serveur. Lorsque PHP traite des valeurs de cookies d’HTTP
entrant, les noms des cookies sont traités comme des URL. Cela pourrait
conduire à des cookies avec des préfixes tels que __Host confondus avec des
cookies décodant de tels préfixes, permettant à un attaquant de falsifier un
cookie supposé fiable.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 7.0.33-0+deb9u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.0, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.0">https://security-tracker.debian.org/tracker/php7.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2397.data"
# $Id: $
