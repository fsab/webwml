#use wml::debian::translation-check translation="600d8ddbe9d22f872c10566e0a10026baefc5c04" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>À cause d’une validation incorrecte d’entrée, Squid est vulnérable à une
attaque par dissimulation de requête HTTP.</p>

<p>Ce problème permet à un client de confiance de réaliser une dissimulation de
requête HTTP et d’accéder à des services autrement interdits par les contrôles
de sécurité de Squid.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.5.23-5+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/squid3">\
https://security-tracker.debian.org/tracker/squid3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2598.data"
# $Id: $
