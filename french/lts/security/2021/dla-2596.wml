#use wml::debian::translation-check translation="70409f71e480d72b7de908db237a7be1c733a7ca" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans la suite d’outils de
connexion shadow. Un attaquant peut augmenter ses privilèges dans des configurations
particulières.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-20002">CVE-2017-20002</a>

<p>Shadow liste de manière incorrecte pts/0 et pts/1 comme des terminaux
physiques dans /etc/securetty. Cela permet à des utilisateurs locaux de se
connecter comme utilisateurs sans mot de passe s’ils sont connectés par des
moyens non matériels tels que SSH (par conséquent en contournant nullok_secure
de la configuration PAM). Cela affecte notamment les environnements tels que
des machines virtuelles générées automatiquement avec un mot de passe
administrateur vierge, permettant à des utilisateurs locaux d’augmenter leurs
privilèges. Il est à noter cependant que /etc/securetty sera supprimé dans
Debian 11, Bullseye.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12424">CVE-2017-12424</a>

<p>L’outil <q>newusers</q> pourrait être utilisé pour manipuler des
structures de données internes d’une matière non souhaitée par les auteurs. Une
entrée mal formée peut conduire à des plantages (avec un dépassement de tampon
ou une autre corruption de mémoire) ou d’autres comportements non précisés. Cela
outrepasse la frontière des privilèges dans, par exemple, certains
environnements d’hébergement web avec un panneau de contrôle permettant à un
compte utilisateur non privilégié de créer des sous-comptes.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:4.4-4.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets shadow.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de shadow, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/shadow">\
https://security-tracker.debian.org/tracker/shadow</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2596.data"
# $Id: $
