#use wml::debian::translation-check translation="fb44169546e6e3162747c207cff4489cb10a9d64" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité d’entité externe XML dans le système
d’authentification unique <em>lemonldap-ng</em>. Cela pouvait conduire à la
divulgation de données confidentielles, un déni de service, une contrefaçon de
requête côté serveur, le balayage de ports, etc.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13031">CVE-2019-13031</a>

<p>LemonLDAP::NG avant 1.9.20 possédait un problème d’entité externe XML (XXE)
lors de la soumission d’une notification au serveur de notifications. Par défaut,
le serveur de notifications n’est pas activé et possède une règle « deny all ».</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.3-1+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets lemonldap-ng.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1844.data"
# $Id: $
