<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A privilege escalation vulnerability was discovered in <a
href="http://www.net-snmp.org/">Net-SNMP</a>, a set of tools for collecting and
organising information about devices on computer networks, due to incorrect
symlink handling (<tt>CVE-2020-15861</tt>).</p>

<p>This security update also applies an upstream fix to their previous handling
of <tt>CVE-2020-15862</tt> as part of <tt>DLA-2299-1</tt>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15861">CVE-2020-15861</a>

    <p>Elevation of Privileges due to symlink handling</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15862">CVE-2020-15862</a>

    <p>privilege escalation</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
5.7.3+dfsg-1.7+deb9u3.</p>

<p>We recommend that you upgrade your net-snmp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2313.data"
# $Id: $
