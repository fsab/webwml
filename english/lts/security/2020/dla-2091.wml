<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were fixed in libjackson-json-java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7525">CVE-2017-7525</a>

    <p>Jackson Deserializer security vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15095">CVE-2017-15095</a>

    <p>Block more JDK types from polymorphic deserialization.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10172">CVE-2019-10172</a>

    <p>XML external entity vulnerabilities.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.9.2-3+deb8u1.</p>

<p>We recommend that you upgrade your libjackson-json-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2091.data"
# $Id: $
