<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to the execution of arbitrary code, privilege escalation,
denial of service or information leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19039">CVE-2019-19039</a>

    <p><q>Team bobfuzzer</q> reported a bug in Btrfs that could lead to an
    assertion failure (WARN).  A user permitted to mount and access
    arbitrary filesystems could use this to cause a denial of service
    (crash) if the panic_on_warn kernel parameter is set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19377">CVE-2019-19377</a>

    <p><q>Team bobfuzzer</q> reported a bug in Btrfs that could lead to a
    use-after-free.  A user permitted to mount and access arbitrary
    filesystems could use this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19770">CVE-2019-19770</a>

    <p>The syzbot tool discovered a race condition in the block I/O
    tracer (blktrace) that could lead to a system crash.  Since
    blktrace can only be controlled by privileged users, the security
    impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19816">CVE-2019-19816</a>

    <p><q>Team bobfuzzer</q> reported a bug in Btrfs that could lead to an
    out-of-bounds write.  A user permitted to mount and access
    arbitrary filesystems could use this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0423">CVE-2020-0423</a>

    <p>A race condition was discovered in the Android binder driver, that
    could result in a use-after-free.  On systems using this driver, a
    local user could use this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8694">CVE-2020-8694</a>

    <p>Multiple researchers discovered that the powercap subsystem
    allowed all users to read CPU energy meters, by default.  On
    systems using Intel CPUs, this provided a side channel that could
    leak sensitive information between user processes, or from the
    kernel to user processes.  The energy meters are now readable only
    by root, by default.</p>

    <p>This issue can be mitigated by running:</p>

        <blockquote><code>chmod go-r /sys/devices/virtual/powercap/*/*/energy_uj</code></blockquote>

    <p>This needs to be repeated each time the system is booted with
    an unfixed kernel version.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14351">CVE-2020-14351</a>

    <p>A race condition was discovered in the performance events
    subsystem, which could lead to a use-after-free.  A local user
    permitted to access performance events could use this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p>

    <p>Debian's kernel configuration does not allow unprivileged users to
    access peformance events by default, which fully mitigates this
    issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25656">CVE-2020-25656</a>

    <p>Yuan Ming and Bodong Zhao discovered a race condition in the
    virtual terminal (vt) driver that could lead to a use-after-free.
    A local user with the <tt>CAP_SYS_TTY_CONFIG</tt> capability could use this
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25668">CVE-2020-25668</a>

    <p>Yuan Ming and Bodong Zhao discovered a race condition in the
    virtual terminal (vt) driver that could lead to a use-after-free.
    A local user with access to a virtual terminal, or with the
    <tt>CAP_SYS_TTY_CONFIG</tt> capability, could use this to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25669">CVE-2020-25669</a>

    <p>Bodong Zhao discovered a bug in the Sun keyboard driver (sunkbd)
    that could lead to a use-after-free.  On a system using this
    driver, a local user could use this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25704">CVE-2020-25704</a>

    <p>kiyin(尹亮) discovered a potential memory leak in the performance
    events subsystem.  A local user permitted to access performance
    events could use this to cause a denial of service (memory
    exhaustion).</p>

    <p>Debian's kernel configuration does not allow unprivileged users to
    access peformance events by default, which fully mitigates this
    issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25705">CVE-2020-25705</a>

    <p>Keyu Man reported that strict rate-limiting of ICMP packet
    transmission provided a side-channel that could help networked
    attackers to carry out packet spoofing.  In particular, this made
    it practical for off-path networked attackers to <q>poison</q> DNS
    caches with spoofed responses ("SAD DNS" attack).</p>

    <p>This issue has been mitigated by randomising whether packets are
    counted against the rate limit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27673">CVE-2020-27673</a> / <a href="https://xenbits.xen.org/xsa/advisory-332.html">XSA-332</a>

    <p>Julien Grall from Arm discovered a bug in the Xen event handling
    code.  Where Linux was used in a Xen dom0, unprivileged (domU)
    guests could cause a denial of service (excessive CPU usage or
    hang) in dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27675">CVE-2020-27675</a> / <a href="https://xenbits.xen.org/xsa/advisory-331.html">XSA-331</a>

    <p>Jinoh Kang of Theori discovered a race condition in the Xen event
    handling code.  Where Linux was used in a Xen dom0, unprivileged
    (domU) guests could cause a denial of service (crash) in dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28941">CVE-2020-28941</a>

    <p>Shisong Qin and Bodong Zhao discovered a bug in the Speakup screen
    reader subsystem.  Speakup assumed that it would only be bound to
    one terminal (tty) device at a time, but did not enforce this.  A
    local user could exploit this bug to cause a denial of service
    (crash or memory exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28974">CVE-2020-28974</a>

    <p>Yuan Ming discovered a bug in the virtual terminal (vt) driver
    that could lead to an out-of-bounds read.  A local user with
    access to a virtual terminal, or with the <tt>CAP_SYS_TTY_CONFIG</tt>
    capability, could possibly use this to obtain sensitive
    information from the kernel or to cause a denial of service
    (crash).</p>

    <p>The specific ioctl operation affected by this bug
    (<tt>KD_FONT_OP_COPY</tt>) has been disabled, as it is not believed that
    any programs depended on it.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.19.160-2~deb9u1.</p>

<p>We recommend that you upgrade your linux-4.19 packages.</p>

<p>For the detailed security status of linux-4.19 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2483.data"
# $Id: $
