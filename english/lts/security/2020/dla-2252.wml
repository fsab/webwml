<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an out-of-bounds access
vulnerability in the server-server protocol in the ngircd Internet
Relay Chat (IRC) server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14148">CVE-2020-14148</a>

    <p>The Server-Server protocol implementation in ngIRCd before 26~rc2 allows
    an out-of-bounds access, as demonstrated by the IRC_NJOIN()
    function.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
22-2+deb8u1.</p>

<p>We recommend that you upgrade your ngircd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2252.data"
# $Id: $
