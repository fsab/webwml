<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A potential HTTP request smuggling vulnerability in WEBrick
was reported.</p>

<p>WEBrick (bundled along with jruby) was too tolerant against
an invalid Transfer-Encoding header. This may lead to
inconsistent interpretation between WEBrick and some HTTP proxy
servers, which may allow the attacker to “smuggle” a request.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.7.26-1+deb9u3.</p>

<p>We recommend that you upgrade your jruby packages.</p>

<p>For the detailed security status of jruby please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jruby">https://security-tracker.debian.org/tracker/jruby</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2392.data"
# $Id: $
