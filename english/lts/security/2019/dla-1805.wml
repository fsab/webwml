<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a use after free vulnerability in minissdpd, a network device discovery daemon. A remote attacker could use this to crash the process.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12106"></a>

    <p>The updateDevice function in minissdpd.c in MiniUPnP MiniSSDPd 1.4 and 1.5 allows a remote attacker to crash the process due to a Use After Free vulnerability.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.2.20130907-3+deb8u2.</p>

<p>We recommend that you upgrade your minissdpd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1805.data"
# $Id: $
