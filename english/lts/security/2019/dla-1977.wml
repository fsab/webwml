<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>LibVNC contained a memory leak (CWE-655) in VNC server code, which
allowed an attacker to read stack memory and could be abused for
information disclosure.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.9.9+dfsg2-6.1+deb8u6.</p>

<p>We recommend that you upgrade your libvncserver packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1977.data"
# $Id: $
