<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An attack vector was discovered by lemonldap-ng developers. When the SAML
or CAS service provider is enable and the administrator has chosen to store
SAML/CAS tokens in the session database, an attacker can open an anonymous
session to connect to any protected application that does not have specific
access rules.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.3-1+deb9u1.</p>

<p>We recommend that you upgrade your lemonldap-ng packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1790.data"
# $Id: $
