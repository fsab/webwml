<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>libvirt-domain.c in libvirt supports virDomainGetTime API calls by guest agents
with an RO connection, even though an RW connection was supposed to be
required.  This could lead to could lead to potentially disclosing unintended
information or denial of service by causing libvirt to block.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.2.9-9+deb8u6.</p>

<p>We recommend that you upgrade your libvirt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1772.data"
# $Id: $
