<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in GraphicsMagick, a set of
command-line applications to manipulate image files, which could result
in information disclosure, denial of service or the execution of
arbitrary code if malformed image files are processed.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1.3.30+hg15796-1~deb9u4.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.4+really1.3.35-1~deb10u1.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>For the detailed security status of graphicsmagick please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/graphicsmagick">\
https://security-tracker.debian.org/tracker/graphicsmagick</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4675.data"
# $Id: $
