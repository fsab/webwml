<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A flaw was discovered in coturn, a TURN and STUN server for VoIP. By
default coturn does not allow peers on the loopback addresses
(127.x.x.x and ::1). A remote attacker can bypass the protection via a
specially crafted request using a peer address of <q>0.0.0.0</q> and trick
coturn in relaying to the loopback interface. If listening on IPv6 the
loopback interface can also be reached by using either [::1] or [::] as
the address.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 4.5.1.1-1.1+deb10u2.</p>

<p>We recommend that you upgrade your coturn packages.</p>

<p>For the detailed security status of coturn please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/coturn">https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4829.data"
# $Id: $
