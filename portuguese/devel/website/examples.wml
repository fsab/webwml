#use wml::debian::template title="Exemplos"
#use wml::debian::translation-check translation="93f96e5508d963eef6dee47f4ac2360e27d7939b" maintainer="Leonardo Rocha"

<H3>Exemplo de como iniciar uma tradução</H3>

<p>O idioma francês será usado como exemplo:

<pre>
   git pull
   cd webwml
   mkdir french
   cd french
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/french,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make* .wmlrc
   cp Makefile po
   make -C po init-po
   git add po/Makefile po/*.fr.po
</pre>

<p>Edite o arquivo <tt>.wmlrc</tt> e altere:
<ul>
  <li>'-D CUR_LANG=English' para '-D CUR_LANG=French'
  <li>'-D CUR_ISO_LANG=en' para '-D CUR_ISO_LANG=fr'
  <li>'-D CUR_LOCALE=en_US' para '-D CUR_LOCALE=fr_FR'
  <li>'-D CHARSET=iso-8859-1' para o que for apropriado.<br>
      O francês coincidentemente usa a mesma codificação de caracteres que
      o inglês, logo nenhuma alteração é necessária. Contudo, é provável
      que novos idiomas precisem ter essa configuração ajustada.
</ul>

<p>Edite o Make.lang e altere 'LANGUAGE := en' para 'LANGUAGE := fr'.
Caso esteja traduzindo para um idioma que usa um conjunto de caracteres
multi-byte, pode ser necessário alterar algumas outras variáveis nesse
arquivo. Para mais informações leia ../Makefile.common e talvez outros
exemplos funcionais (traduções como a chinesa).

<p>Vá para french/po e traduza as entradas nos arquivos PO. Isso deve
ser bastante simples.

<p>Sempre certifique-se de copiar o Makefile para cada diretório que você
traduzir. Isso é necessário porque o programa <code>make</code> é usado para
converter os arquivos .wml em HTML, e o <code>make</code> usa Makefiles.

<p>Quando terminar de adicionar e editar as páginas, faça um
<pre>
   git commit -m "Adicione aqui uma messagem de commit"
   git push
</pre>
a partir do diretório webwml. Você agora pode começar a traduzir as páginas.

<H3>Exemplo de tradução de uma página</H3>

<p>Uma tradução francesa do contrato social será usada como exemplo:

<pre>
   cd webwml
   ./copypage.pl english/social_contract.wml
   cd french
</pre>

<p>Isso irá automaticamente adicionar o cabeçalho translation-check, apontando
para a versão do arquivo original que foi copiado. Isso também cria o diretório
de destino e o Makefile, se estiver faltando.</p>

<p>Edite o arquivo social_contract.wml e traduza o texto. Não tente traduzir
quaisquer links ou alterá-los de qualquer modo - se você deseja mudar alguma
coisa, solicite-a na lista debian-www. Quando terminar, execute

<pre>
   git add social_contract.wml
   git commit -m "Translated social contract to french"
   git push
</pre>

<H3>Exemplo da adição de um novo diretório</H3>

<p>Este exemplo mostra a adição do diretório intro/ na tradução francesa:

<pre>
   cd webwml/french
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m "added the intro dir to git"
   git push
</pre>

Certifique-se que o novo diretório tem o Makefile e que seu commit foi feito
no git. Caso contrário, executar o make dará um erro para todos os outros
que tentarem.

#exemplo não finalizado
# <H3>Exemplo de um conflito</H3>
#
# <p>Este exemplo mostra um commit que não funcionará porque a cópia do
# repositório foi modificada desde seu último <kbd>git pull</kbd>.
#
# <pre>
#    git foo.wml
#    cvs commit -m "fixed a broken link"
#    git push
# </pre>
#
# a saída será:
#
# <pre>
#To salsa.debian.org:webmaster-team/webwml.git
# ! [rejected]                master -> master (fetch first)
#error: failed to push some refs to 'git@salsa.debian.org:webmaster-team/webwml.git'
# </pre>
#
# ou algo semelhante :)
