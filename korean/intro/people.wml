#use wml::debian::template title="사람들: 우리는 누구, 우리는 무엇을 하는가"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="bbb1b67054b9061c9420f1c5826b2fa85f05c738" maintainer="Sebul" 

# translators: some text is taken from /intro/about.wml

<h2>개발자 및 기여자</h2>
<p>데비안은 여가 시간에 자원봉사를 하는 <a href="$(DEVEL)/developers.loc">전세계</a>에 있는 
거의 천 명의 활동적인 개발자가 만듭니다. 
실제로 직접 만난 개발자는 거의 없습니다. 
소통은 주로 이메일(lists.debian.org의 뉴스레터 목록)과 IRC(irc.debian.org의 #debian 채널)를 통해 이루어집니다.
</p>

<p>공식 데비안 멤버의 완전한 목록은
<a href="https://nm.debian.org/members">nm.debian.org</a>에서 찾을 수 있으며, 여기서 멤버쉽을 관리합니다. 
데비안 기여자 더 넓은 목록은 
<a href="https://contributors.debian.org">contributors.debian.org</a>에서 찾을 수 있습니다.</p>

<p>데비안 프로젝트는 조심스럽게 <a href="organization">조직된 구조</a>를 가집니다.
안에서 어떻게 데비안이 보이는지 정보는 
<a href="$(DEVEL)/">개발자 코너</a> 보셔요.</p>

<h3><a name="history">어떻게 모든 게 시작되었나요?</a></h3>

<p>데비안은 1993년 8월 이안 머독이 리눅스와 GNU의 정신으로 공개적으로 만들어질 새로운 배포판으로 시작했습니다. 
데비안은 신중하고 양심적으로 합쳐지고, 비슷한 보살핌으로 유지되고 지지받도록 되어 있었습니다. 
이 조직은 소규모의 긴밀한 자유 소프트웨어 해커 그룹으로 시작했으며 
점차 개발자와 사용자들로 구성된 대규모 조직화된 커뮤니티로 성장했습니다. 
<a href="$(DOC)/manuals/project-history/">자세한 역사</a>를 보세요.

<p>여러 사람이 물어서 대답하는데, 데비안은 /ˈde.bi.ən/ 으로 발음합니다. 
데비안 창시자 Ian Murdock, 그리고 그 아내 Debra에서 왔습니다.
  
<h2>데비안을 지원하는 개인 및 기관</h2>

<p>많은 다른 개인과 기관이 데비안 커뮤니티의 일부입니다:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">호스팅 및 하드웨어 후원</a></li>
  <li><a href="../mirror/sponsors">미러 후원</a></li>
  <li><a href="../partners/">개발 및 서비스 파트너</a></li>
  <li><a href="../consultants">컨설턴드</a></li>
  <li><a href="../CD/vendors">데비안 설치 미디어 업체</a></li>
  <li><a href="../distrib/pre-installed">데비안이 미리 설치된 컴퓨터 업체</a></li>
  <li><a href="../events/merchandise">제작 업체</a></li>
</ul>

<h2><a name="users">누가 데비안을 쓰나요?</a></h2>

<p>정확한 통계는 없지만 (데비안은 사용자 등록할 필요가 없으므로) 데비안을 크고 작은 다양한 조직과 수천 명의 개인에서 사용한다는 증거는 꽤 강력합니다. 
어떻게 왜 데비안을 쓰는지 간단한 설명을 제출한 유명 조직 목록 페이지 및 <a href="../users/">누가 데비안을 쓰나요?</a> 페이지를 보세요.

