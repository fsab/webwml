#use wml::debian::template title="Πληροφορίες έκδοσης του Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="c8265f664931f5ce551446ba751d63d63b70ce86" maintainer="galaxico"

<if-stable-release release="bullseye">

<p>Το Debian <current_release_bullseye> κυκλοφόρησε στις <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Το Debian 11.0 κυκλοφόρησε αρχικά στις <:=spokendate('XXXXXXXX'):>."
/>
Η έκδοση περιελάμβανε πολλές μείζονες αλλαγές, 
που περιγράφονται στο <a href="$(HOME)/News/XXXX/XXXXXXXX">Δελτίο Τύπου</a> και τις <a href="releasenotes">Σημειώσεις της Έκδοσης</a>.</p>

#<p><strong>Debian 11 has been superseded by
#<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, bullseye benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in bullseye.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>Για να αποκτήσετε και να εγκαταστήσετε το Debian, δείτε τη σελίδα <a href="debian-installer/">πληροφορίες εγκατάστασης</a> και τον
<a href="installmanual">Οδηγό Εγκατάστασης</a>. Για να κάνετε αναβάθμιση από μια παλιότερη έκδοση του Debian, δείτε τις οφηγίες στις
<a href="releasenotes">Σημειώσεις της Έκδοσης</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Υπολογιστικές αρχιτεκτονικές που υποστηρίζονται από την αρχική κυκλοφορία της έκδοσης bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
<a href="reportingbugs">report other issues</a> to us.</p>

<p>Last but not least, we have a list of <a href="credits">people who take
credit</a> for making this release happen.</p>
</if-stable-release>

<if-stable-release release="buster">

<p>Η κωδική ονομασία της επόμενης μείζονος έκδοσης του Debian μετά από την <a
href="../buster/">buster</a> είναι <q>bullseye</q>.</p>

<p>Αυτή η έκδοση ξεκίνησε σαν μια κόπια της έκδοσης buster και είναι προς το παρόν σε κατάσταση <q><a href="$(DOC)/manuals/debian-faq/ftparchives#δοκιμαστική">δοκιμαστική</a></q>.
Αυτό σημαίνει ότι πράγματα δεν "χαλάνε" τόσο άσχημα όσο στην ασταθή ή την πειραματική διανομή, επειδή τα πακέτα μπορούν να μπουν σ' αυτήν μόνο αφού έχει περάσει ένα χρονικό διάστημα και όταν δεν έχουν κανένα από τα κρίσιμα για την κυκλοφορία της διανομής σφάλματα που έχουν δηλωθεί σε σχέση με αυτά.</p>

<p>Παρακαλούμε σημειώστε ότι τις επικαιροποιήσεις για την <q>δοκιμαστική</q> διανομή δεν τις διαχειρίζεται ακόμα η ομάδα ασφαλείας. Συνεπώς, η <q>δοκιμαστική</q> διανομή
<strong>δεν</strong> λαμβάνει επικαιροποιήσεις ασφαλείας έγκαιρα.
# For more information please see the
# <a href="https://lists.debian.org/debian-δοκιμαστική-security-announce/2008/12/msg00019.html">announcement</a>
# of the δοκιμαστική Security Team.
Σας ενθαρρύνουμε να αλλάξετε, προς τα παρόν, τις σχετικές εισόδους στο αρχείο sources.list από την δοκιμαστική στην buster αν χρειάζεστε υποστήριξη ασφαλείας. Δείτε επίσης τα σχετικά στη σελίδα
<a href="$(HOME)/security/faq#δοκιμαστική">Συχνές Ερωτήσεις της Ομάδας Ασφαλείας</a> για την <q>δοκιμαστική</q> διανομή.</p>

<p>Ίσως υπάρχει διαθέσιμο ένα <a href="releasenotes">προσχέδιο των σημειώσεων της έκδοσης</a>. Παρακαλούμε, επίσης <a href="https://bugs.debian.org/release-notes">ελέγξτε τις προτεινόμενες προσθήκες στις σημειώσεις της έκδοσης</a>.</p>

<p>Για εικόνες εγκατάστασης και τεκμηρίωση σχετικά με το πώς να εγκαταστήσετε τη <q>δοκιμαστική</q> διανομή,
δείτε τη σελίδα του <a href="$(HOME)/devel/debian-installer/">Εγκαταστάτη του Debian</a>.</p>

<p>Για να βρείτε περισσότερα για το πώς "δουλεύει" η  <q>δοκιμαστική</q> διανομή, ελέγξτε <a href="$(HOME)/devel/δοκιμαστική">τις πληροφορίες των προγραμματιστ(ρι)ών</a> σχετικά με αυτήν.</p>

<p>Πολύς κόσμος ρωτά αν υπάρχει ένας μοναδικός <q>μετρητής προόδου</q> της έκδοσης. Δυστυχώς δεν υπάρχει κάτι τέτοιο, αλλά μπορούμε να σας παραπέμψουμε σε αρκετά σημεία που περιγράφουν πράγματα που πρέπει να αντιμετωπιστούν ώστε να γίνει η κυκοφορία της έκδοσης:</p>

<ul>
  <li><a href="https://release.debian.org/">Σελίδα της γενικής κατάστασης της έκδοσης</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Σφάλματα κρίσιμα για την κυκλοφορία της έκδοσης</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Σφάλματα του βασικού συστήματος</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Σφάλματα στα βασικά πακέτα και τα πακέτα καθηκόντων (tasks)</a></li>
</ul>

<p>Επιπλέον, αναφορές για τη γενική κατάσταση της έκδοσης αναρτώνται από τον διαχειριστή της κυκλοφορίας της έκδοσης στην λίστα αλληλογραφίας <a href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce</a>.</p>

</if-stable-release>
